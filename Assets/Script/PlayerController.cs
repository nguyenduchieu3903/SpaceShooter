using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public PlayerHealthBar healthBar;
    public float health;
    private float m_Health;
    // Start is called before the first frame update
    void Start()
    {
        m_Health= health;

        healthBar.SetMaxHeath(health);
    }

    // Update is called once per frame
    void Update()
    {
        if (m_Health <= 0)
        {
            Death();
        }
        healthBar.SetHeath(m_Health);
    }

    void TakeDamage(float amout)
    {
        m_Health -= amout;
    }

    void Death()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "EnemyBullet")
        {
            TakeDamage(1f);
        }
    }
}
