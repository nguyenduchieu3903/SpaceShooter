using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using static UnityEditor.Experimental.GraphView.GraphView;
using static UnityEngine.GraphicsBuffer;

public class Enemy : MonoBehaviour
{
    //public Ship ship;
    public float speed;
    public int enemyHP;
    [SerializeField] public Enemy move;
    public GameObject[] target;
    private GameObject m_currentTarget;
    private int m_currentTargetIndex;

    void Start()
    {
        
    }

    private void Update()
    {
        //transform.position += new Vector3(speed, speed, 0);
        //transform.Translate(Vector2.down * speed * Time.deltaTime);

        m_currentTarget = target[m_currentTargetIndex];
        if (Vector3.Distance(transform.position, m_currentTarget.transform.position) < 0.1f)
        {
            m_currentTargetIndex++;

            if (m_currentTargetIndex > target.Length - 1)
            {

                m_currentTargetIndex = 0;
            }
            m_currentTarget = target[m_currentTargetIndex];


        }


        transform.position = Vector3.Lerp(transform.position, m_currentTarget.transform.position, speed);
    }

    public void TakeDamage(int damage)
    {
        enemyHP -= damage;
        if (enemyHP <= 0)
        {
            Destroy(gameObject);
            //ship.score++;
        }
    }
}
