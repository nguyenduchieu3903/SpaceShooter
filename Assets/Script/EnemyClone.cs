using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class EnemyClone : MonoBehaviour
{
    public GameObject enemyLD;

    public float enemySpawnRate;
    private float _EnemySpawnRate;

    public Enemy move;
    [SerializeField] public Enemy target;
    //public Enemy m_currentarget;

    // Start is called before the first frame update
    void Start()
    {
        //_EnemySpawnRate = enemySpawnRate;

        StartCoroutine(SpawnEnemy());
    }

    // Update is called once per frame
    void Update()
    {
        /*if(_EnemySpawnRate <= 0f)
        {
            SpawnEnemy();
            _EnemySpawnRate = enemySpawnRate;
        }
        else
        {
            _EnemySpawnRate -= Time.deltaTime;
        }*/
    }
    IEnumerator SpawnEnemy()
    {
        //Instantiate(enemyLD, new Vector3(Random.Range(minX, maxX), 0f, transform.position.z), transform.rotation);
        /*yield return new WaitForSeconds(1f);
        GameObject clone = Instantiate(enemyLD, new Vector3(-1.75f, 5.5f, 0), Quaternion.identity);

        yield return new WaitForSeconds(1.3f);
        GameObject clone2 = Instantiate(enemyLD, new Vector3(1.75f, 5.5f, 0), Quaternion.identity);

        yield return new WaitForSeconds(1.5f);
        GameObject clone3 = Instantiate(enemyLD, new Vector3(-0.75f, 5.5f, 0), Quaternion.identity);

        yield return new WaitForSeconds(1.7f);
        GameObject clone4 = Instantiate(enemyLD, new Vector3(0.75f, 5.5f, 0), Quaternion.identity);
        */
        yield return new WaitForSeconds(1f);
        GameObject clone = Instantiate(enemyLD, new Vector3(0, 0, 0), Quaternion.identity);
        Enemy enemyMove = clone.GetComponent<Enemy>();
        enemyMove.move = move;
        //enemyMove.target = target;

        StartCoroutine(SpawnEnemy());
    }
}
