using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.Experimental.GraphView.GraphView;
using UnityEngine.UI;
using UnityEngine.UIElements;
using static UnityEngine.GraphicsBuffer;


public class Shoot : MonoBehaviour
{
    public Ship ship;

    [SerializeField] private GameObject _laser;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Shooting());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator Shooting()
    {
        yield return new WaitForSeconds(0.2f);

        Vector3 temp = transform.position;
        temp.y += 0.5f;

        GameObject laser = Instantiate(_laser, transform.position, Quaternion.identity);
        LaserBullet LazerControler = laser.GetComponent<LaserBullet>();
        LazerControler.ship = ship;

        StartCoroutine(Shooting());
    }
}

