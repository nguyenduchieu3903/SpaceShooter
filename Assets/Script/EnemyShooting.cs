using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour
{
    public Transform firePoint;
    public float shootSpeed;
    private float shootSpeedTime;

    [SerializeField] private GameObject _enemyLaser;
    // Start is called before the first frame update
    void Start()
    {
        //shootSpeedTime = shootSpeed;

        StartCoroutine(EnemyShoot());
    }

    // Update is called once per frame
    void Update()
    {
        /*if (shootSpeedTime <= 0f)
        {
            Shoot();
            shootSpeedTime = shootSpeed;
        }
        else
        {
            shootSpeedTime -= Time.deltaTime;
        }*/
    }

    void Shoot()
    {
        //GameObject enemyBullet = Instantiate(_enemyLaser, firePoint.position, firePoint.rotation) as GameObject;
        //Destroy(enemyBullet, 2f);
    }

    IEnumerator EnemyShoot()
    {
        yield return new WaitForSeconds(1f);

        //Vector3 temp = transform.position;
        //temp.y -= 2f;

        Vector3 T = transform.position;
        GameObject enemyBullet = Instantiate(_enemyLaser, T, Quaternion.identity);

        Destroy(enemyBullet, 2f);

        StartCoroutine(EnemyShoot());
    }
}
