using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipBounds : MonoBehaviour
{
    private float _minX, _maxX, _minY, _maxY;
    // Start is called before the first frame update
    void Start()
    {
        Vector3 bounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        _minX = -bounds.x +0.1f;
        _maxX = bounds.x -0.1f;

        _minY = -bounds.y +0.1f;
        _maxY = bounds.y -0.1f;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 temp = transform.position;

        if(temp.x < _minX)
        {
            temp.x = _minX;
        }
        else if (temp.x > _maxX)
        {
            temp.x = _maxX;
        }

        if (temp.y < _minY)
        {
            temp.y = _minY;
        }
        else if (temp.y > _maxY)
        {
            temp.y = _maxY;
        }

        transform.position = temp;
    }
}
