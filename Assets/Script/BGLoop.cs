using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGLoop : MonoBehaviour
{
    public float speed;

    private Vector2 _offset = Vector2.zero;
    private Material _mat;

    void Start()
    {
        _mat= GetComponent<Renderer>().material;
        _offset = _mat.GetTextureOffset("_MainTex");
    }

    // Update is called once per frame
    void Update()
    {
        _offset.y += speed * Time.deltaTime;
        _mat.SetTextureOffset("_MainTex", _offset);
    }
}
