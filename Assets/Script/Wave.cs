using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Wave : MonoBehaviour
{
    public string name;
    public Transform enemyLD;
    public int enemyCount;
    public FlowState spawnRate;
}
