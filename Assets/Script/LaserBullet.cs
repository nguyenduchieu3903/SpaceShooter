using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class LaserBullet : MonoBehaviour
{
    public Ship ship;
    public float speed;
    public float timer = 1f;

    private Rigidbody2D _myLaser;
    // Start is called before the first frame update
    void Awake()
    {
        _myLaser = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        _myLaser.velocity = new Vector2(_myLaser.velocity.x, speed);
    }

    private void Update()
    {
        timer -= Time.deltaTime;
        if(timer <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "enemy")
        {
            //Enemy enemy = target.GetComponent<Enemy>();
            //if (enemy != null)
            //{
            //    enemy.TakeDamage(1);
            //    ship.score++;
            //}
            Destroy(gameObject);
            Destroy(collision.gameObject);
        }
        
        
    }

}
